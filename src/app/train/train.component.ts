import { Component, OnInit } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
// import { DrawableDirective } from './drawable.directive';


@Component({
  selector: 'app-train',
  templateUrl: './train.component.html',
  styleUrls: ['./train.component.css']
})
export class TrainComponent implements OnInit {

  linearModel: tf.Sequential;
  prediction: any;

  Beta0 = 3;
  Beta1 = 8;
  constructor() { }

  traning_set = {
    x: [],
    y: [],
  }

  generateRandomNumber(theVariableCount){
    for(var x=0; x< theVariableCount; x++){
      // console.log('x:', x)
      const val = Math.random()*10;
      this.traning_set.x.push(val)
      this.traning_set.y.push(this.Beta0 + this.Beta1 * val)
    }
    console.log('traning_set', this.traning_set)
  }


  ngOnInit() {
    this.linearModel = tf.sequential();
    this.linearModel.add(tf.layers.dense({units: 1, inputShape: [1]}));
    // Prepare the model for training: Specify the loss and the optimizer.
    this.linearModel.compile({loss: 'meanSquaredError', optimizer: 'sgd'});
  }

 Training(){
    // Training data, completely random stuff
    this.linearModel.fit(
      tf.tensor1d(this.traning_set.x),
      tf.tensor1d(this.traning_set.y)
      )
    console.log('model trained!')
  }

  predictValue(theVal){
    // console.log("theVal", theVal)
    // console.log("typeof theVal",typeof theVal)
    let output = this.linearModel.predict(tf.tensor1d([Number(theVal)])) as tf.Tensor
    // output.print()
    // console.log("data", output.data())
    // console.log("dataSync", )
    this.prediction = output.dataSync()[0]
  }

  TEST(){
    console.log(this.linearModel.outputs)

    // let output = this.linearModel.predict(tf.tensor1d([5])) as tf.Tensor

    // output.print()

    // console.log("data", output.data())
    // console.log("dataSync", output.dataSync()[0])

    // Asrray.from(output.dataSync())[0]
  }

}
